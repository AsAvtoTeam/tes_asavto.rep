<div class="uk-container uk-container-center uk-padding-collapse">
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"big-slider1", 
	array(
		"IBLOCK_TYPE" => "data",
		"IBLOCK_ID" => $GLOBALS["CONFIG"]["IBLOCKS"]["slider"],
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_BY2" => "",
		"SORT_ORDER2" => "",
		"FILTER_NAME" => "ACTIONS_SLIDER_FILTER",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "MOBILE_IMAGE",
			1 => "IS_VIEW",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "#ELEMENT_CODE#",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"ZK_MODIFIER" => "",
		"ZK_HEIGHT" => "400",
		"ZK_LINK" => "",
		"COMPONENT_TEMPLATE" => "big-slider1",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "Y",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
</div>
<div class="uk-container uk-container-center uk-grid-collapse uk-grid uk-hidden-small slider-line">
	<div class="uk-width-1-2 uk-grid-collapse uk-grid" style="background:#efefef;padding: 7px 0 0;">
		<div class="uk-grid uk-grid-collapse" style="padding-left:10px;"><div class="uk-h1 uk-text-bold">№1</div><div class="uk-text-small" style="margin:0;">Официальный дилер <br>китайских автомобилей  </div></div>
		<div class="uk-grid uk-grid-collapse"><div class="uk-h1 uk-text-bold">15</div><div class="uk-text-small uk-text-center">лет на <br>рынке</div></div>
		<div class="uk-grid uk-grid-collapse"><div class="uk-h1 uk-text-bold">25 000</div><div class="uk-text-small">новых а/м <br>продано</div></div>
	</div>
	<div class="uk-width-1-2 uk-grid-collapse uk-grid" style="background:#efefef;padding: 7px 0 0;">
		<div class="uk-grid uk-grid-collapse"><div class="uk-h1 uk-text-bold">15</div><div class="uk-text-small">банков-<br>партнеров</div></div>
		<div class="uk-grid uk-grid-collapse"><div class="uk-h1 uk-text-bold">> 1 МЛН.</div><div class="uk-text-small">ТО <br>выполнено</div></div>
		<div class="uk-grid uk-grid-collapse"><div class="uk-h1 uk-text-bold">4</div><div class="uk-text-small">дилерских центра в <br>3-х городах</div></div>
	</div>
</div>
